package com.tiwweek6;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot peter = new Robot("Peter", 'P', 10, 10);
        body.print();
        body.right();
        body.print();
        peter.up();
        body.print();

    }
}
