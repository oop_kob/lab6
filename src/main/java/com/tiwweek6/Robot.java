package com.tiwweek6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int X_MAX = 19;
    public final static int Y_MIN = 0;
    public final static int Y_MAX = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        
    }
    
    public void print() {
        System.out.println(name = "x:" + x + " y:" + y);
    }
    
    //Getter Setter Method
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean up() {
        if(y==Y_MIN) return false;
        y = y - 1;
        return true;
    }

    public boolean down() {
        if(y==Y_MAX) return false;
        y = y + 1;
        return true;
    }

    public boolean left() {
        if(x==X_MIN) return false;
        x = x - 1;
        return true;
    }

    public boolean right() {
        if(x==X_MAX) return false;
        x = x + 1;
        return true;
    }
}
