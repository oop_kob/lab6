package com.tiwweek6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("Worawit", 100);
        worawit.print();
        worawit.deposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(0);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(0);
        praweet.print();
        
    }

}
